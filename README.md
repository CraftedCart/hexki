HexKi
=====

A hex viewer and analysis framework.

![Screenshot](https://i.imgur.com/hUNCgb4.png)

## Bundled plugins

### highlight_tools
Lists highlighted regions on a hex buffer

### super_monkey_ball_stagedef_tools (WIP)
Parses Super Monkey Ball 2 stage definition formats and presents the information in a list, while also allowing easy highlighting of the data.

## Writing your own plugins
Currently there is no plugin loading framework besides the ones bundled with HexKi. For now, add yout plugin in hexki/bundledplugins, and append it to the `__all__` list in hexki/bundledplugins/\_\_init\_\_.py
