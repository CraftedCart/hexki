#!/usr/bin/env python3

from pathlib import Path
from typing import List
import traceback
import sys

import glfw
from OpenGL import GL

import imgui
from imgui.integrations.glfw import GlfwRenderer

from hexki.hexki import HexKi
from hexki.workspace import Workspace
from hexki.ui.hex_editor import HexEditor
from hexki.buffer import Buffer

# Load all bundled plugins
# noinspection PyUnresolvedReferences
from hexki.bundledplugins import *


def main():
    HexKi.init_instance()

    window = impl_glfw_init()
    imgui.create_context()
    impl = GlfwRenderer(window)

    # GlfwRenderer doesn't map this for some reason...
    imgui.get_io().key_map[imgui.KEY_SPACE] = glfw.KEY_SPACE
    # although pyimgui doesn't expose config flags for imgui io, so I can't enable keyboard nav >.<

    workspace = Workspace()

    for file in sys.argv[1:]:
        buffer = Buffer()
        buffer.read_from_file(file)
        HexKi.instance.buffers.append(buffer)
        workspace.editors.append(HexEditor(buffer, workspace))

    while not glfw.window_should_close(window):
        glfw.poll_events()
        impl.process_inputs()

        imgui.new_frame()

        workspace.draw()

        GL.glClearColor(0.13, 0.13, 0.13, 1)
        GL.glClear(GL.GL_COLOR_BUFFER_BIT)

        imgui.render()
        impl.render(imgui.get_draw_data())
        glfw.swap_buffers(window)

    impl.shutdown()
    glfw.terminate()


def impl_glfw_init():
    width, height = 1280, 720
    window_name = "HexKi"

    if not glfw.init():
        print("Could not initialize OpenGL context")
        sys.exit(1)

    # macOS supports only forward-compatible core profiles from 3.2
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
    glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, GL.GL_TRUE)

    # Create a windowed mode window and its OpenGL context
    window = glfw.create_window(
        int(width), int(height), window_name, None, None
    )
    glfw.make_context_current(window)
    glfw.swap_interval(1)  # Enable VSync

    if not window:
        glfw.terminate()
        print("Could not initialize Window")
        sys.exit(1)

    return window


if __name__ == "__main__":
    main()
