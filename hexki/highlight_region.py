from hexki.vectors import Vec3


class HighlightRegion:
    def __init__(self, start: int, end: int, color: Vec3, name: str = ""):
        self.start = start
        self.end = end
        self.color = color
        self.name = name
