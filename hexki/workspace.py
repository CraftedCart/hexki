from typing import List
import sys
import traceback
import imgui
from pathlib import Path
from hexki.hexki import HexKi
from hexki import workspace_tool
from hexki.buffer import Buffer
from hexki.ui.file_picker import FilePicker
from hexki.ui.hex_editor import HexEditor
from hexki.utils.option import Option


class Workspace:
    def __init__(self):
        self.editors: List[HexEditor] = []

        self.workspace_tools: List = []
        self.picker: Option[FilePicker] = Option.none()

        self.show_test_window = False
        self.show_metrics_window = False

    def file_selected(self, file_path: str):
        self.picker = Option.none()
        buffer = Buffer()
        buffer.read_from_file(file_path)
        HexKi.instance.buffers.append(buffer)
        self.editors.append(HexEditor(buffer, self))

    def remove_editor(self, editor: HexEditor):
        self.editors.remove(editor)

    def draw(self):
        if imgui.begin_main_menu_bar():
            if imgui.begin_menu("File"):
                if imgui.menu_item("Open")[0]:
                    self.picker = Option.some(FilePicker(str(Path.home())))
                    self.picker.unwrap().wired_connect(FilePicker.file_selected, self.file_selected)

                if imgui.menu_item("Quit")[0]:
                    sys.exit(0)

                imgui.end_menu()

            if imgui.begin_menu("New workspace tool"):
                for tool in workspace_tool.workspace_tools:
                    if imgui.menu_item(tool.name)[0]:
                        self.workspace_tools.append(tool())

                imgui.end_menu()

            if imgui.begin_menu("Buffers"):
                for buffer in HexKi.instance.buffers:
                    if imgui.menu_item(buffer.name)[0]:
                        self.editors.append(HexEditor(buffer, self))

                imgui.end_menu()

            if imgui.begin_menu("Help"):
                self.show_test_window = imgui.menu_item("Show test window", selected=self.show_test_window)[1]
                self.show_metrics_window = imgui.menu_item("Show metrics window", selected=self.show_metrics_window)[1]

                if imgui.begin_menu("Conventions"):
                    imgui.text("H Highlight")
                    imgui.text("> Jump to offset")
                    imgui.end_menu()

                imgui.end_menu()

            # FPS display on the right of the menu bar
            imgui.same_line(imgui.get_window_width() - 100)
            if imgui.button(f"FPS: {imgui.get_io().framerate:.2f}###main_menu_bar_fps"):
                self.show_metrics_window = not self.show_metrics_window

            imgui.end_main_menu_bar()

        if self.show_test_window:
            imgui.show_test_window()
        if self.show_metrics_window:
            imgui.show_metrics_window()

        if self.picker.is_some():
            self.picker.unwrap().draw()

        # Draw editors
        for editor in self.editors:
            editor.draw()

        # Draw buffer tools
        for buffer in HexKi.instance.buffers:
            for tool in buffer.tools:
                # noinspection PyBroadException
                try:
                    tool.draw()
                except:
                    traceback.print_exc()
                    imgui.text("Buffer tool {} is throwing exceptions".format(tool.name))

        # Draw editor
        for tool in self.workspace_tools:
            # noinspection PyBroadException
            try:
                tool.draw()
            except:
                traceback.print_exc()
                imgui.text("Workspace tool {} is throwing exceptions".format(tool.name))
