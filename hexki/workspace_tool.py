from typing import List

workspace_tools: List = []


# Decorator
def workspace_tool(func):
    workspace_tools.append(func)
    return func
