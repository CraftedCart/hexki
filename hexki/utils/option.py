from typing import Generic, TypeVar, Any, Optional


T = TypeVar("T")


class Option(Generic[T]):
    def __init__(self, value: Optional[T]):
        self.value: Optional[T] = value

    def unwrap(self) -> T:
        if self.value is None:
            raise RuntimeError("Tried to unwrap a None Option")
        else:
            return self.value

    def expect(self, message) -> T:
        if self.value is None:
            raise RuntimeError(message)
        else:
            return self.value

    def is_some(self) -> bool:
        return self.value is not None

    def is_none(self) -> bool:
        return self.value is None

    def __str__(self) -> str:
        return str(self.value)

    def __repr__(self) -> str:
        if self.value is None:
            return "option.none()"
        else:
            return "option.some(" + str(self.value) + ")"

    @staticmethod
    def some(value: Any) -> "Option[Any]":
        assert(value is not None)
        return Option(value)

    @staticmethod
    def none() -> "Option[None]":
        return Option(None)
