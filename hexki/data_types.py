from hexki.utils.option import Option
import struct
from typing import Callable, List


class DataType:
    def __init__(self, name: str, length: Option[int], formatter: Callable, getter: Callable):
        self._name = name
        self._length = length
        self._formatter = formatter
        self._getter = getter

    @property
    def name(self) -> str:
        return self._name

    @property
    def length(self) -> Option[int]:
        return self._length

    def format_value(self, value: bytes) -> str:
        return self._formatter(value)

    def get_value(self, value: bytes):
        return self._getter(value)


class ArrayDataType:
    def __init__(self, elem_type: DataType, array_length: int):
        self.elem_type = elem_type
        self.array_length = array_length

    @property
    def name(self) -> str:
        return f"{self.elem_type.name}[{self.array_length}]"

    @property
    def length(self) -> Option[int]:
        if self.elem_type.length.is_none():
            return None

        return Option.some(self.elem_type.length.unwrap() * self.array_length)

    def format_value(self, value: bytes) -> str:
        s = "["

        elems_formatted = []
        for i in range(self.array_length):
            elem_data = value[i * self.elem_type.length.unwrap():(i + 1) * self.elem_type.length.unwrap()]
            elems_formatted.append(self.elem_type.format_value(elem_data))

        s += ", ".join(elems_formatted)
        s += "]"

        return s

    def get_value(self, value: bytes) -> List:
        elem_values = []
        for i in range(self.array_length):
            elem_data = value[i * self.elem_type.length.unwrap():(i + 1) * self.elem_type.length.unwrap()]
            elem_values.append(self.elem_type.format_value(elem_data))

        return elem_values


def make_array_type(elem_type: DataType, length: int) -> ArrayDataType:
    return ArrayDataType(elem_type, length)


# Formatters
def format_undefined(value: bytes) -> str:
    return "???"


def format_hex(value: bytes) -> str:
    return "0x" + value.hex().upper()


def format_hex_reverse(value: bytes) -> str:
    swap_data = bytearray(value)
    swap_data.reverse()
    return "0x" + swap_data.hex().upper()


def format_bool(value: bytes) -> str:
    if value[0] == 0:
        return "false"
    else:
        return "true"


def _read_to_null(value: bytes) -> bytes:
    trimmed = []
    for b in value:
        if b == 0:
            break

        trimmed.append(b)

    return bytes(trimmed)


def format_ascii_str_z(value: bytes) -> str:
    return _read_to_null(value).decode("ascii", "replace")


def format_utf8_str_z(value: bytes) -> str:
    return _read_to_null(value).decode_zeroterm("utf-8", "replace")


# Populated with numerical formatters lower down...
formatters = {
    "hex": format_hex,
    "bool": format_bool,
    "ascii_str_z": format_ascii_str_z,
    "utf8_str_z": format_utf8_str_z,
}

# Getters
# What a useful function
def get_bytes(value: bytes):
    return value


def get_bool(value: bytes):
    return value[0] != 0


# Populated with numerical getters lower down...
getters = {
    "bytes": get_bytes,
    "bool": get_bool,
    "ascii_str_z": format_ascii_str_z,
    "utf8_str_z": format_utf8_str_z,
}

# Populated with numerical types lower down...
data_types = {
    "undefined": DataType(name="undefined", length=Option.none(), formatter=format_undefined, getter=get_bytes),
    "undefined1": DataType(name="undefined1", length=Option.some(1), formatter=format_hex, getter=get_bytes),
    "undefined2": DataType(name="undefined2", length=Option.some(2), formatter=format_hex, getter=get_bytes),
    "undefined4": DataType(name="undefined4", length=Option.some(4), formatter=format_hex, getter=get_bytes),
    "undefined8": DataType(name="undefined8", length=Option.some(8), formatter=format_hex, getter=get_bytes),
    "undefined16": DataType(name="undefined16", length=Option.some(16), formatter=format_hex, getter=get_bytes),
    "undefined32": DataType(name="undefined32", length=Option.some(32), formatter=format_hex, getter=get_bytes),
    "undefined64": DataType(name="undefined64", length=Option.some(64), formatter=format_hex, getter=get_bytes),
    "bool": DataType(name="bool", length=Option.some(1), formatter=format_bool, getter=get_bool),
    "ascii_str_z": DataType(name="ascii_str_z", length=Option.none(), formatter=format_ascii_str_z, getter=format_ascii_str_z),
    "utf8_str_z": DataType(name="utf8_str_z", length=Option.none(), formatter=format_utf8_str_z, getter=format_utf8_str_z),
}


# Add numerical types
_num_types = [
    ("i8", 1, "b"),
    ("u8", 1, "B"),
    ("i16", 2, "h"),
    ("u16", 2, "H"),
    ("i32", 4, "i"),
    ("u32", 4, "I"),
    ("i64", 8, "q"),
    ("u64", 8, "Q"),
    ("f32", 4, "f"),
    ("f64", 8, "d"),
]

_ptr_types = [
    ("uptr8", 1, "b"),
    ("uptr16", 2, "h"),
    ("uptr32", 4, "i"),
    ("uptr64", 8, "q"),
]


def register_num_type(t):
    le_name = t[0] + "_le"
    be_name = t[0] + "_be"

    def le_getter(value: bytes):
        return struct.unpack("<" + t[2], value)[0]

    def be_getter(value: bytes):
        return struct.unpack(">" + t[2], value)[0]

    getters[le_name] = le_getter
    getters[be_name] = be_getter

    def le_formatter(value: bytes):
        return str(struct.unpack("<" + t[2], value)[0])

    def be_formatter(value: bytes):
        return str(struct.unpack(">" + t[2], value)[0])

    formatters[le_name] = le_formatter
    formatters[be_name] = be_formatter

    le_type = DataType(name=le_name, length=Option.some(t[1]), formatter=le_formatter, getter=le_getter)
    be_type = DataType(name=be_name, length=Option.some(t[1]), formatter=be_formatter, getter=be_getter)

    data_types[le_name] = le_type
    data_types[be_name] = be_type


for t in _num_types:
    register_num_type(t)


def register_ptr_type(t):
    le_name = t[0] + "_le"
    be_name = t[0] + "_be"

    def le_getter(value: bytes):
        return struct.unpack("<" + t[2], value)[0]

    def be_getter(value: bytes):
        return struct.unpack(">" + t[2], value)[0]

    getters[le_name] = le_getter
    getters[be_name] = be_getter

    le_type = DataType(name=le_name, length=Option.some(t[1]), formatter=format_hex_reverse, getter=le_getter)
    be_type = DataType(name=be_name, length=Option.some(t[1]), formatter=format_hex, getter=be_getter)

    data_types[le_name] = le_type
    data_types[be_name] = be_type


for t in _ptr_types:
    register_ptr_type(t)


del _num_types
