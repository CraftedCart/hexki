import colorsys
import os
from typing import List

import imgui

from hexki.files import File, Directory
from hexki.wired import Wired


class FilePicker(Wired):
    def __init__(self, dir_path: str):
        self.dir_path: str = ""
        self.dir_entries: List[File] = []

        self.show_hidden = False

        self.set_dir_path(dir_path)

    def set_dir_path(self, dir_path: str):
        self.dir_path = os.path.abspath(dir_path)
        self.dir_entries.clear()

        files = os.listdir(dir_path)

        for f in files:
            if os.path.isdir(os.path.join(dir_path, f)):
                self.dir_entries.append(Directory(f))
            else:
                self.dir_entries.append(File(f))

        self.dir_entries.sort()

    def draw(self) -> None:
        imgui.begin("File picker", True)

        if imgui.button(".."):
            self.set_dir_path(os.path.dirname(self.dir_path))

        imgui.same_line()

        # Draw our current location
        if self.dir_path.endswith(os.sep):
            imgui.text(self.dir_path)
        else:
            imgui.text(self.dir_path + os.sep)

        imgui.separator()

        _, self.show_hidden = imgui.checkbox("Show hidden files", self.show_hidden)

        imgui.separator()

        # Draw each entry button
        columns = max(1, int(imgui.get_content_region_available_width() / 256))
        imgui.columns(columns)

        for entry in self.dir_entries:
            if entry.hidden and not self.show_hidden:
                continue

            name = entry.name
            if isinstance(entry, Directory):
                name += os.sep
                is_dir = True

                imgui.push_style_color(imgui.COLOR_BUTTON, *colorsys.hsv_to_rgb(0.09, 0.6, 0.6))
                imgui.push_style_color(imgui.COLOR_BUTTON_HOVERED, *colorsys.hsv_to_rgb(0.09, 0.7, 0.7))
                imgui.push_style_color(imgui.COLOR_BUTTON_ACTIVE, *colorsys.hsv_to_rgb(0.09, 0.8, 0.8))

            else:
                is_dir = False

            if imgui.button(name):
                if is_dir:
                    self.set_dir_path(os.path.join(self.dir_path, entry.name))
                else:
                    # Clicked on a file
                    self.file_selected(os.path.join(self.dir_path, entry.name))

            if is_dir:
                imgui.pop_style_color(3)

            imgui.next_column()

        imgui.columns(1)

        imgui.end()

    def file_selected(self, file_path: str):
        self.wired_signal_emit(file_path)
