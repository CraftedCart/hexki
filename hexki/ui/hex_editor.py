import math
import traceback
from typing import Optional

import imgui

from hexki import buffer_tool
from hexki.buffer import Buffer
from hexki.highlight_region import HighlightRegion
from hexki.ui import imguiw
from hexki.vectors import Vec3


class HexEditor:
    def __init__(self, buffer: Buffer, workspace: "Workspace"):
        self.buffer = buffer
        self.pending_scroll_offset: Optional[float] = None
        self.width_bytes = 16
        self.workspace = workspace

        self.jump_to_input = 0
        self.jump_to_hex_input = "0"
        self.highlight_jump = True

        buffer.wired_connect(Buffer.jump_to_offset, self.jump_to_offset)

    def draw(self) -> None:
        with imguiw.begin(f"Hex editor: {self.buffer.name}###{id(self)}", True, imgui.WINDOW_MENU_BAR | imgui.WINDOW_NO_SAVED_SETTINGS) as s:
            if not s.opened:
                self.workspace.remove_editor(self)
            else:
                # Top menu
                imgui.begin_menu_bar()
                if imgui.begin_menu("New buffer tool"):
                    for tool in buffer_tool.buffer_tools:
                        if imgui.menu_item(tool.name)[0]:
                            self.buffer.tools.append(tool(self.buffer))

                    imgui.end_menu()
                imgui.end_menu_bar()

                # Top widgets
                # Jump to offset
                jump_to_changed, self.jump_to_input = imgui.input_int("Jump to (b10)", self.jump_to_input)
                imgui.same_line()
                jump_to_button = imgui.button(">###jump_to_offset_b10")
                imgui.same_line()
                highlight_jump_button, self.highlight_jump = imgui.checkbox("H###highlight_jump_b10", self.highlight_jump)
                jump_to_hex_changed, self.jump_to_hex_input = imgui.input_text("Jump to (b16)", self.jump_to_hex_input, 9)
                imgui.same_line()
                jump_to_hex_button = imgui.button(">###jump_to_offset_b16")
                imgui.same_line()
                highlight_jump_hex_button, self.highlight_jump = imgui.checkbox("H###highlight_jump_b16", self.highlight_jump)

                if highlight_jump_button or highlight_jump_hex_button:
                    if self.highlight_jump:
                        # Highlight the jumped position
                        region = HighlightRegion(self.jump_to_input, self.jump_to_input, Vec3(0.2, 1, 0.2), "jump_to_offset")
                        self.buffer.highlight_regions.insert(0, region)
                    else:
                        region = self.buffer.get_highlight_region("jump_to_offset")
                        if region is not None:
                            self.buffer.highlight_regions.remove(region)

                if jump_to_changed:
                    self.jump_to_input = max(self.jump_to_input, 0)
                    self.jump_to_hex_input = format(max(self.jump_to_input, 0), "X")
                elif jump_to_hex_changed:
                    try:
                        self.jump_to_input = min(max(int(self.jump_to_hex_input, 16), 0), 0x7FFFFFFF)
                        self.jump_to_hex_input = format(min(max(int(self.jump_to_hex_input, 16), 0), 0x7FFFFFFF), "X")
                    except ValueError:
                        pass

                if jump_to_button or jump_to_hex_button or jump_to_changed or jump_to_hex_changed:
                    self.pending_scroll_offset = self.jump_to_input

                    if self.highlight_jump:
                        # Highlight the jumped position
                        region = self.buffer.get_highlight_region("jump_to_offset")
                        if region is not None:
                            self.buffer.highlight_regions.remove(region)

                        region = HighlightRegion(self.jump_to_input, self.jump_to_input, Vec3(0.2, 1, 0.2), "jump_to_offset")
                        self.buffer.highlight_regions.insert(0, region)

                # Draw the hex editor
                line_num_digits = len(format(len(self.buffer.data), "X"))
                with imguiw.begin_child("HexEditor", imgui.get_content_region_available_width(), False, None):
                    scroll_offset = abs(imgui.get_window_content_region_min().y)
                    lines_offset = math.floor(scroll_offset / imgui.get_text_line_height())

                    offset = lines_offset * self.width_bytes

                    # Initial spacing
                    imgui.push_style_var(
                        imgui.STYLE_ITEM_SPACING,
                        imgui.Vec2(0, (offset / self.width_bytes) * imgui.get_text_line_height())
                    )
                    imgui.spacing()
                    imgui.pop_style_var()

                    # Remove spacing between bytes
                    imgui.push_style_var(imgui.STYLE_ITEM_SPACING, imgui.Vec2(0, 0))

                    stride = 1
                    for i in range(offset, offset + min(4096, len(self.buffer.data) - offset), self.width_bytes):
                        # See if we should scroll here
                        if self.pending_scroll_offset is not None and i > self.pending_scroll_offset:
                            imgui.set_scroll_here()
                            self.pending_scroll_offset = None

                        # The offset line numbers on the left
                        imgui.text("{offset: {digits}X}".format(offset=i, digits=line_num_digits + 1))
                        imgui.same_line()
                        imgui.text_colored(" | ".format(offset=i, digits=line_num_digits), 1, 1, 1, 0.4)
                        imgui.same_line()

                        # The hex data in the middle
                        j = i
                        for b in self.buffer.data[i:i + self.width_bytes]:
                            # Check if this byte should be colored
                            col = self.buffer.get_offset_highlight_color(j)
                            if col is None:
                                # No highlight
                                if b == 0:
                                    imgui.text_colored("{:02X} ".format(b), 1, 1, 1, 0.4)
                                else:
                                    imgui.text("{:02X} ".format(b))
                            else:
                                # There is a highlight
                                imgui.text_colored("{:02X} ".format(b), *col)

                            # Some extra spacing every 4 bytes
                            if stride % 4 == 0:
                                imgui.same_line()
                                imgui.text(" ")

                            imgui.same_line()

                            stride += 1
                            j += 1

                        imgui.text_colored("| ", 1, 1, 1, 0.4)
                        imgui.same_line()

                        # The text part on the tight
                        for b in self.buffer.data[i:i + self.width_bytes]:
                            if b == 0 or chr(b) == "\t":
                                imgui.text(".")
                            else:
                                imgui.text(chr(b))

                            if stride % 16 != 0:
                                imgui.same_line()

                            stride += 1

                    imgui.pop_style_var()

                    # Fill the rest with spacing
                    imgui.push_style_var(
                        imgui.STYLE_ITEM_SPACING,
                        imgui.Vec2(
                            0,
                            ((len(self.buffer.data) - offset) / self.width_bytes - 4096 / self.width_bytes) * imgui.get_text_line_height()
                        )
                    )
                    # I need this twice because reasons
                    imgui.spacing()
                    imgui.spacing()
                    imgui.pop_style_var()

    def jump_to_offset(self, offset: int):
        self.pending_scroll_offset = offset
