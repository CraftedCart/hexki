import imgui
from collections import namedtuple


WindowState = namedtuple("WindowState", ["expanded", "opened"])

class Window:
    # noinspection PyPep8Naming
    def __init__(self, str_name, closable=False, ImGuiWindowFlags_flags=0):
        self.str_name = str_name
        self.closable = closable
        self.ImGuiWindowFlags_flags = ImGuiWindowFlags_flags

    def __enter__(self) -> WindowState:
        return WindowState(*imgui.begin(self.str_name, self.closable, self.ImGuiWindowFlags_flags))

    def __exit__(self, exc_type, exc_val, exc_tb):
        imgui.end()


# noinspection PyPep8Naming
def begin(str_name, closable=False, ImGuiWindowFlags_flags=0) -> Window:
    return Window(str_name, closable, ImGuiWindowFlags_flags)


class Menu:
    def __init__(self, str_label, enabled=True):
        self.str_label = str_label
        self.enabled = enabled

    def __enter__(self) -> bool:
        self.menu_open = imgui.begin_menu(self.str_label, self.enabled)
        return self.menu_open

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.menu_open:
            imgui.end_menu()


def begin_menu(str_label, enabled=True) -> Menu:
    return Menu(str_label, enabled)


class MenuBar:
    def __enter__(self):
        imgui.begin_menu_bar()

    def __exit__(self, exc_type, exc_val, exc_tb):
        imgui.end_menu_bar()


def begin_menu_bar():
    return MenuBar()


class MainMenuBar:
    def __enter__(self):
        imgui.begin_main_menu_bar()

    def __exit__(self, exc_type, exc_val, exc_tb):
        imgui.end_main_menu_bar()


def begin_main_menu_bar():
    return MainMenuBar()


class CollapsingHeader:
    # noinspection PyPep8Naming
    def __init__(self, str_text, visible, ImGuiTreeNodeFlags_flags=0):
        self.str_text = str_text
        self.visible = visible
        self.ImGuiTreeNodeFlags = ImGuiTreeNodeFlags_flags

    def __enter__(self):
        return imgui.collapsing_header(self.str_text, self.visible, self.ImGuiTreeNodeFlags)[0]

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


# noinspection PyPep8Naming
def collapsing_header(str_text, visible, ImGuiTreeNodeFlags_flags=0) -> CollapsingHeader:
    return CollapsingHeader(str_text, visible, ImGuiTreeNodeFlags_flags)


class TreeNode:
    # noinspection PyPep8Naming
    def __init__(self, str_text, ImGuiTreeNodeFlags_flags=0):
        self.str_text = str_text
        self.ImGuiTreeNodeFlags = ImGuiTreeNodeFlags_flags

    def __enter__(self):
        self.tree_open = imgui.tree_node(self.str_text, self.ImGuiTreeNodeFlags)
        return self.tree_open

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.tree_open:
            imgui.tree_pop()


# noinspection PyPep8Naming
def tree_node(str_text, ImGuiTreeNodeFlags_flags=0) -> TreeNode:
    return TreeNode(str_text, ImGuiTreeNodeFlags_flags)


class Columns:
    def __init__(self, int_count=1, str_identifier=None, bool_border=True):
        self.int_count = int_count
        self.str_identifier = str_identifier
        self.bool_border = bool_border

    def __enter__(self):
        self.current_columns = imgui.get_columns_count()
        imgui.columns(self.int_count, self.str_identifier, self.bool_border)

    def __exit__(self, exc_type, exc_val, exc_tb):
        imgui.columns(self.current_columns)


def columns(int_count=1, str_identifier=None, bool_border=True) -> Columns:
    return Columns(int_count, str_identifier, bool_border)


class Child:
    def __init__(self, signatures, args, kwargs, defaults):
        self.signatures = signatures
        self.args = args
        self.kwargs = kwargs
        self.defaults = defaults

    def __enter__(self):
        self.current_columns = imgui.get_columns_count()
        imgui.begin_child(self.signatures, self.args, self.kwargs, self.defaults)

    def __exit__(self, exc_type, exc_val, exc_tb):
        imgui.end_child()


def begin_child(signatures, args, kwargs, defaults) -> Child:
    return Child(signatures, args, kwargs, defaults)

# TODO: The rest of everything
