import imgui

from hexki.workspace_tool import workspace_tool
from hexki import data_types
from hexki.ui import imguiw


@workspace_tool
class DataTypeTools:
    name = "Data type tools"

    def draw(self):
        with imguiw.begin(f"Data types###{self}", False, imgui.WINDOW_NO_SAVED_SETTINGS):
            with imguiw.columns(2):
                # Headings
                imgui.text("Name")
                imgui.next_column()
                imgui.text("Length")
                imgui.next_column()

                imgui.separator()

                for k, v in data_types.data_types.items():
                    imgui.text(v.name)
                    imgui.next_column()
                    imgui.text(str(v.length))
                    imgui.next_column()
