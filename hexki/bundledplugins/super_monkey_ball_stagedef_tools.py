import colorsys
import random
import struct
from typing import Tuple

import imgui

from hexki.buffer import Buffer
from hexki.buffer_tool import buffer_tool
from hexki.highlight_region import HighlightRegion
from hexki.ui import imguiw
from hexki.vectors import Vec3
from hexki.utils.option import Option
from hexki.data_types import DataType, data_types, make_array_type


vec2_f32_type = make_array_type(data_types["f32_be"], 2)
vec2_u32_type = make_array_type(data_types["u32_be"], 2)
vec3_f32_type = make_array_type(data_types["f32_be"], 3)
vec3_u16_type = make_array_type(data_types["u16_be"], 3)


class Section:
    def __init__(self, id: str, offset: int, data_type: Option[DataType] = Option.none()):
        self.id = id
        self.rel_offset = offset
        self.data_type: Option[DataType] = data_type
        self.children = []
        self.parent = Option.none()

    def add_child(self, child: "Section") -> "Section":
        self.children.append(child)
        child.parent = Option.some(self)
        return child

    @property
    def path(self):
        path_elems = [self.id]
        parent = self.parent
        while parent.is_some():
            path_elems.insert(0, parent.unwrap().id)
            parent = parent.unwrap().parent

        return "/".join(path_elems)

    @property
    def abs_offset(self):
        offset = self.rel_offset
        parent = self.parent
        while parent.is_some():
            offset += parent.unwrap().rel_offset
            parent = parent.unwrap().parent

        return offset


    def get_value(self, buffer):
        return self.data_type.unwrap().get_value(
            buffer.data[self.abs_offset:self.abs_offset + self.data_type.unwrap().length.unwrap()]
            )


@buffer_tool
class SuperMonkeyBallStagedefTools:
    name = "Super Monkey Ball Stagedef Tools"

    def __init__(self, buffer: Buffer):
        self.buffer = buffer
        self.indent_width = 0

    def find_sections(self) -> Section:
        root = Section("root", 0x0)

        file_header = root.add_child(Section("file_header", 0x0))
        file_header.add_child(Section("magic_number", 0x0, Option.some(data_types["undefined8"])))
        collision_group_count = file_header.add_child(Section("collision_group_count", 0x8, Option.some(data_types["u32_be"]))).get_value(self.buffer)
        collision_group_list_offset = file_header.add_child(Section("collision_group_list_offset", 0xC, Option.some(data_types["uptr32_be"]))).get_value(self.buffer)
        start_offset = file_header.add_child(Section("start_offset", 0x10, Option.some(data_types["uptr32_be"]))).get_value(self.buffer)
        fallout_offset = file_header.add_child(Section("fallout_offset", 0x14, Option.some(data_types["uptr32_be"]))).get_value(self.buffer)
        file_header.add_child(Section("goal_count", 0x18, Option.some(data_types["u32_be"])))
        file_header.add_child(Section("goal_list_offset", 0x1C, Option.some(data_types["uptr32_be"])))
        file_header.add_child(Section("bumper_count", 0x20, Option.some(data_types["u32_be"])))
        file_header.add_child(Section("bumper_list_offset", 0x24, Option.some(data_types["uptr32_be"])))
        file_header.add_child(Section("jamabar_count", 0x28, Option.some(data_types["u32_be"])))
        file_header.add_child(Section("jamabar_list_offset", 0x2C, Option.some(data_types["uptr32_be"])))
        file_header.add_child(Section("banana_count", 0x30, Option.some(data_types["u32_be"])))
        file_header.add_child(Section("banana_list_offset", 0x34, Option.some(data_types["uptr32_be"])))
        file_header.add_child(Section("cone_collision_count", 0x38, Option.some(data_types["u32_be"])))
        file_header.add_child(Section("cone_collision_list_offset", 0x3C, Option.some(data_types["uptr32_be"])))
        file_header.add_child(Section("sphere_collision_count", 0x40, Option.some(data_types["u32_be"])))
        file_header.add_child(Section("sphere_collision_list_offset", 0x44, Option.some(data_types["uptr32_be"])))
        file_header.add_child(Section("cylinder_collision_count", 0x48, Option.some(data_types["u32_be"])))
        file_header.add_child(Section("cylinder_collision_list_offset", 0x4C, Option.some(data_types["uptr32_be"])))
        file_header.add_child(Section("fallout_volume_count", 0x50, Option.some(data_types["u32_be"])))
        file_header.add_child(Section("fallout_volume_list_offset", 0x54, Option.some(data_types["uptr32_be"])))
        file_header.add_child(Section("background_model_count", 0x58, Option.some(data_types["u32_be"])))
        file_header.add_child(Section("background_model_list_offset", 0x5C, Option.some(data_types["uptr32_be"])))
        file_header.add_child(Section("mystery_8_count", 0x60, Option.some(data_types["u32_be"])))
        file_header.add_child(Section("mystery_8_list_offset", 0x64, Option.some(data_types["uptr32_be"])))
        # Unknown at 0x64 for 0x4 bytes
        file_header.add_child(Section("0x00000001_0", 0x6C, Option.some(data_types["u32_be"])))
        file_header.add_child(Section("runtime_reflective_count", 0x70, Option.some(data_types["u32_be"])))
        file_header.add_child(Section("runtime_reflective_list_offset", 0x74, Option.some(data_types["uptr32_be"])))
        # Unknown at 0x78 for 0xC bytes
        # TODO

        start = root.add_child(Section("start", start_offset))
        start.add_child(Section("position", 0x0, Option.some(vec3_f32_type)))
        start.add_child(Section("rotation", 0xC, Option.some(vec3_u16_type)))

        fallout = root.add_child(Section("fallout", fallout_offset))
        fallout.add_child(Section("y", 0x0, Option.some(data_types["f32_be"])))

        collision_group_list = root.add_child(Section("collision_group_list", collision_group_list_offset))
        for i in range(collision_group_count):
            collision_group = collision_group_list.add_child(Section(f"collision_group_{i}", 0x49C * i))
            collision_group.add_child(Section("center_of_rotation", 0x0, Option.some(vec3_f32_type)))
            collision_group.add_child(Section("initial_rotation", 0xC, Option.some(vec3_u16_type)))
            collision_group.add_child(Section("anim_loop_type_seesaw", 0x12, Option.some(data_types["u16_be"])))
            collision_group.add_child(Section("animation_header_offset", 0x14, Option.some(data_types["uptr32_be"])))
            collision_group.add_child(Section("conveyor_speed", 0x18, Option.some(vec3_f32_type)))
            collision_group.add_child(Section("collision_triangle_list_offset", 0x24, Option.some(data_types["uptr32_be"])))
            collision_group.add_child(Section("collision_grid_triangle_list_offset", 0x28, Option.some(data_types["uptr32_be"])))
            collision_group.add_child(Section("collision_grid_start", 0x2C, Option.some(vec2_f32_type)))
            collision_group.add_child(Section("collision_grid_step", 0x34, Option.some(vec2_f32_type)))
            collision_group.add_child(Section("collision_grid_count", 0x3C, Option.some(vec2_u32_type)))
            # TODO

        return root

    def draw_section(self, section: Section):
        if section.data_type.is_none():
            with imguiw.collapsing_header(f"{section.id} @ 0x{section.abs_offset:08X}###{section.path}", True) as s:
                if s:
                    self.indent()
                    with imguiw.columns(6):
                        self.setup_columns()
                        self.add_column_headers()

                    for child in section.children:
                        self.draw_section(child)
                    self.unindent()

        else:
            with imguiw.columns(6):
                self.setup_columns()

                type_length = section.data_type.unwrap().length
                if type_length.is_some():
                    type_length = type_length.unwrap()
                    type_length_str = "0x%X" % type_length
                else:
                    type_length = 0
                    type_length_str = "None"

                path = section.path
                abs_offset = section.abs_offset
                self.draw_highlight_button(path, abs_offset, type_length)
                imgui.same_line()
                self.draw_jump_button(path, abs_offset)
                imgui.next_column()
                imgui.text(section.id)
                imgui.next_column()
                imgui.text(
                    section.data_type.unwrap().format_value(
                        self.buffer.data[section.abs_offset:section.abs_offset + type_length]
                        )
                    )
                imgui.next_column()
                imgui.text("0x%X" % abs_offset)
                imgui.next_column()
                imgui.text(type_length_str)
                imgui.next_column()
                imgui.text(section.data_type.unwrap().name)
                imgui.next_column()

    def draw(self):
        # We add self in the window title so each instance has a unique title
        with imguiw.begin("Super Monkey Ball Stagedef Tools###{}".format(self), True, imgui.WINDOW_NO_SAVED_SETTINGS) as s:
            if not s.opened:
                self.buffer.remove_tool(self)
            else:
                root = self.find_sections()
                self.draw_section(root)

    def indent(self):
        self.indent_width += 16
        imgui.indent(16)

    def unindent(self):
        self.indent_width -= 16
        imgui.unindent(16)

    def setup_columns(self):
        # Column zero is where buttons go
        imgui.set_column_offset(1, 50 + self.indent_width)
        imgui.set_column_offset(2, 256 + self.indent_width)
        imgui.set_column_offset(3, 512 + self.indent_width)
        imgui.set_column_offset(4, 512 + 128 + self.indent_width)
        imgui.set_column_offset(5, 512 + 256 + self.indent_width)

    @staticmethod
    def add_column_headers():
        imgui.text("H | >")
        imgui.next_column()
        imgui.text("Item")
        imgui.next_column()
        imgui.text("Value")
        imgui.next_column()
        imgui.text("Offset")
        imgui.next_column()
        imgui.text("Length")
        imgui.next_column()
        imgui.text("Type")
        imgui.next_column()

        imgui.separator()

    def draw_highlight_button(self, unique_id: str, offset: int, length: int) -> None:
        highlight_region = self.buffer.get_highlight_region(unique_id)

        if highlight_region is None:
            button = imgui.small_button("H###" + unique_id)
        else:
            imgui.push_style_color(imgui.COLOR_BUTTON, *highlight_region.color)
            imgui.push_style_color(
                imgui.COLOR_BUTTON_HOVERED,
                highlight_region.color.r + 0.2,
                highlight_region.color.g + 0.2,
                highlight_region.color.b + 0.2
            )
            button = imgui.small_button("H###HighlightRegion:" + unique_id)
            imgui.pop_style_color(2)

        if button:
            # Toggle the region on button click
            if highlight_region is None:
                color = Vec3(*colorsys.hsv_to_rgb(random.uniform(0, 1), 0.8, 1))

                self.buffer.highlight_regions.append(
                    HighlightRegion(offset, offset + length - 1, color, unique_id)
                )
            else:
                self.buffer.highlight_regions.remove(highlight_region)

        # Is the highlight button hovered?
        hover_region = self.buffer.get_highlight_region("hover:" + unique_id)
        if imgui.is_item_hovered():
            if hover_region is None:
                self.buffer.highlight_regions.insert(
                    0,
                    HighlightRegion(offset, offset + length - 1, Vec3(1, 1, 0.2), "hover:" + unique_id)
                )
        else:
            if hover_region is not None:
                self.buffer.highlight_regions.remove(hover_region)

    def draw_jump_button(self, unique_id: str, offset: int) -> None:
        if imgui.small_button(">###jump_to_offset:" + unique_id):
            self.buffer.jump_to_offset(offset)
