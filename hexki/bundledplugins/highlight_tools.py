import imgui

from hexki.buffer import Buffer
from hexki.buffer_tool import buffer_tool
from hexki.ui import imguiw


@buffer_tool
class HighlightTools:
    name = "Highlight Tools"

    def __init__(self, buffer: Buffer):
        self.buffer: Buffer = buffer

    def draw(self):
        # We add self in the window title so each instance has a unique title
        with imguiw.begin("Highlight Tools###{}".format(self), True, imgui.WINDOW_NO_SAVED_SETTINGS) as s:
            if not s.opened:
                self.buffer.remove_tool(self)
            else:
                with imguiw.columns(4):
                    self.setup_columns()

                    i = 0
                    for region in self.buffer.highlight_regions:
                        imgui.text(region.name)
                        imgui.next_column()
                        imgui.text("0x{:08X}".format(region.start))
                        imgui.next_column()
                        imgui.text("0x{:08X}".format(region.end))
                        imgui.next_column()
                        imgui.color_button(region.name, *region.color)
                        imgui.next_column()

                        i += 1

    @staticmethod
    def setup_columns():
        imgui.text("Name")
        imgui.next_column()
        imgui.text("Start")
        imgui.next_column()
        imgui.text("End")
        imgui.next_column()
        imgui.text("Color")
        imgui.next_column()

        imgui.separator()
