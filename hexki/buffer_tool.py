from typing import List

buffer_tools: List = []


# Decorator
def buffer_tool(func):
    buffer_tools.append(func)
    return func
