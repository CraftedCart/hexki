import asyncio
import inspect
from typing import Callable, Dict, List


class Wired:
    """
    Base class for other wired classes
    """

    def wired_signal_emit(self, *args, **kwargs) -> None:
        signal_name = inspect.stack()[1][3]

        if not hasattr(self, "wired_connections") or signal_name not in self.wired_connections:
            # No connections
            return

        for signal_name, slots in self.wired_connections.items():
            for slot in slots:
                if slot.async_loop is None:
                    slot.method(*args, **kwargs)
                else:
                    slot.async_loop.create_task(slot.method(*args, **kwargs))

    def wired_connect(self, signal: Callable, slot: Callable, async_loop=None) -> None:
        if not hasattr(self, "wired_connections"):
            # noinspection PyAttributeOutsideInit
            self.wired_connections: Dict[str, List[SlotConnection]] = {}

        if signal.__name__ not in self.wired_connections:
            self.wired_connections[signal.__name__] = [SlotConnection(slot, async_loop=async_loop)]
        else:
            self.wired_connections[signal.__name__].append(SlotConnection(slot, async_loop=async_loop))

    def wired_connect_async(self, signal: Callable, slot: Callable, async_loop=asyncio.get_event_loop()) -> None:
        """
        Convenience function to just use the default loop by default
        """

        self.wired_connect(signal, slot, async_loop=async_loop)


class SlotConnection:
    def __init__(self, method: Callable, async_loop=None):
        self.method = method
        self.async_loop = async_loop
