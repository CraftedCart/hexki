class File:
    def __init__(self, name: str):
        self.name = name
        self.hidden = name.startswith(".")

    def __lt__(self, other: "File"):
        return self.name < other.name


class Directory(File):
    def __init__(self, name: str):
        super().__init__(name)
