class Vec2:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @property
    def r(self):
        return self.x

    @property
    def g(self):
        return self.y

    def __iter__(self):
        yield self.x
        yield self.y


class Vec3:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    @property
    def r(self):
        return self.x

    @property
    def g(self):
        return self.y

    @property
    def b(self):
        return self.z

    def __iter__(self):
        yield self.x
        yield self.y
        yield self.z


class Vec4:
    def __init__(self, x, y, z, w):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

    @property
    def r(self):
        return self.x

    @property
    def g(self):
        return self.y

    @property
    def b(self):
        return self.z

    @property
    def a(self):
        return self.a

    def __iter__(self):
        yield self.x
        yield self.y
        yield self.z
        yield self.w
