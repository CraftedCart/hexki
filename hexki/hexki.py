from typing import List
from hexki.buffer import Buffer
import glfw


class HexKi:
    instance: "Optional[HexKi]" = None

    def __init__(self):
        self.buffers: List[Buffer] = []

    @staticmethod
    def init_instance():
        HexKi.instance = HexKi()
