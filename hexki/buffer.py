import os
from typing import List, Optional

from hexki.highlight_region import HighlightRegion
from hexki.vectors import Vec3
from hexki.wired import Wired


class Buffer(Wired):
    def __init__(self):
        self.name = "Unnamed"
        self.data = bytearray()
        self.highlight_regions: List[HighlightRegion] = []
        self.tools: List = []

    def read_from_file(self, file_path: str) -> None:
        with open(file_path, "rb") as f:
            self.name = os.path.basename(file_path)
            self.data = bytearray(f.read())

    def get_offset_highlight_color(self, offset: int) -> Optional[Vec3]:
        for region in self.highlight_regions:
            if region.start <= offset <= region.end:
                return region.color

        # Found nothing
        return None

    def get_highlight_region(self, name: str) -> Optional[HighlightRegion]:
        for region in self.highlight_regions:
            if region.name == name:
                return region

        # Nothing found
        return None

    def jump_to_offset(self, offset: int):
        self.wired_signal_emit(offset)

    def remove_tool(self, tool):
        self.tools.remove(tool)
